import { useState, useEffect } from 'react'
/************************/
/*      Components      */
import Header from './components/Header'
import AddProduct from './components/AddProduct'
import ProductTable from './components/ProductTable'
import EditForm from './components/EditForm'
import CategoryFilter from './components/CategoryFilter'
import PriceFilter from './components/PriceFilter'

const App = () => { 
  const [products, setProducts] = useState([])
  const [filteredProducts, setFilteredProducts] = useState([])
  const [catFilter, setCatFilter] = useState('all')
  const [priceFilter, setPriceFilter] = useState([])

  const baseUrl = 'http://localhost:8000/products'

  useEffect(() => {
    const getProducts = async () => {
      const productsFromServer = await fetchProducts()
      setProducts(productsFromServer)
    }
    getProducts()
  }, [])

  useEffect(() => {
    setFilteredProducts(products.filter(product => {
      // Nach Kategorien und Preisen filtern
      if(catFilter !== 'all' && (priceFilter.length > 0)) {  
        return product.kategorie === catFilter && product.preis > priceFilter[0] && product.preis < priceFilter[1]
      }
      // Nur nach Kategorien filtern
      if(catFilter !== 'all' && (priceFilter.length === 0)) {
        return product.kategorie === catFilter
      }
      // Nur nach Priesen filtern
      if(catFilter === 'all' && (priceFilter.length > 0 )) {
        return  product.preis > priceFilter[0] && product.preis < priceFilter[1]
      }
      // Ohne zu filtern
      return true
      
    }))
  },[products, catFilter, priceFilter])
  
  const fetchProducts = async () => {
    const res = await fetch(baseUrl)
    const data = await res.json()
    return data
  } 



  // const fetchProduct = async id => {
  //   const res = await fetch(`${baseUrl}/${id}`)
  //   const data = await res.json()
  //   return data
  // } 

  // Da verhindern wir das Formular abzusenden, mit Enter zu tippen
  // um autocomplete Input richtig funktionieren zu können
  const noEnter = () => {
    window.addEventListener('keydown', event => {      
      if(event.key === 'Enter') {
        event.preventDefault()
        return false
      }
    })
  }
  noEnter()

  const meldung = (string, art = 'success') => {
    let element = document.createElement('div')
    element.className = `alert alert-${art}`
    element.setAttribute('role', 'alert')
    element.textContent = string
    document.querySelector('.table-container').prepend(element)
    setTimeout(function() {
       element.remove()        
    }, 5000)   
  }

  const addProduct = async product => {
    if(!product.bezeichnung || !product.beschreibung || !product.preis || !product.kategorie) {
      meldung('Füllen Sie das Formular vollständig aus!', 'danger')
      return
    }     
    for(let vorhandenesProduct of products) {
      if(vorhandenesProduct.bezeichnung === product.bezeichnung) {
        meldung('Dieses Produkt wurde bereits hinzugefügt! Schreiben Sie bitte eine andere Bezeichnung', 'danger')
        return        
      }
    }
    const response = await fetch(baseUrl, {
      method: 'POST',
      headers: {
        'Content-type': 'application/json'
      },
      body: JSON.stringify(product)
    })

    const newProduct = await response.json()
    setProducts([...products, newProduct]) 
    meldung('Das Produkt wurde erfolgreich hinzugefügt!')   
  }

  const deleteProduct = async id => {
    await fetch(`${baseUrl}/${id}`, {method: 'Delete'})
    setProducts( products.filter(product => product.id !== id) )
    meldung('Das Produkt wurde erfolgreich gelöscht!') 
  }

  const editProduct = async product => {
    if(!product.bezeichnung || !product.beschreibung || !product.preis || !product.kategorie) {
      meldung('Füllen Sie das Formular vollständig aus!', 'danger')
      return
    }    
    const updProduct = {
      id: product.id,
      bezeichnung: product.bezeichnung,
      preis: product.preis,
      beschreibung: product.beschreibung,
      kategorie: product.kategorie
    }
    await fetch(`${baseUrl}/${product.id}`, {
      method: 'PUT',
      headers: {
        'Content-type': 'application/json'
      },
      body: JSON.stringify(updProduct)
    })    
    
    setProducts( products.map( item => {
      return (item.id === product.id) ? updProduct : item
    }))
    meldung('Das Produkt wurde erfolgreich bearbeitet!')
  }
  const kategorienArray = products.map(product => product.kategorie)  
  const uniqueKategorien = [...new Set(kategorienArray)]
  
  return (
    <div className="App">
      <Header />
      <main className="container">
        <div className="row">
          <div className="col-md-4">
           <AddProduct onAdd={addProduct} kategorien={uniqueKategorien}/>
           <CategoryFilter kategorien={uniqueKategorien} setCatFilter={setCatFilter}/>
           <PriceFilter setPriceFilter={setPriceFilter}/>
          </div>
          <div className="col-md-8 table-container">
            <EditForm onEdit={editProduct} kategorien={uniqueKategorien}/>
            <ProductTable products={filteredProducts} onDelete={deleteProduct}/>
          </div>
        </div>
      </main>
    </div>
  );
}

export default App;
