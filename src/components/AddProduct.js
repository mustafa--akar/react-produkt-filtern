import ToggleFormButton from './ToggleFormButton'
import AddProductForm from './AddProductForm'

const AddProduct = ({ onAdd, kategorien }) => {
    const onFormularToggle = () => {
        const form = document.querySelector('.produkt-hinzufuegen-form')
        const button = document.querySelector('.btn.btn-form-toggle')
        const icon = button.querySelector('i')

        if(button.classList.contains('btn-success')) {
            button.classList.remove('btn-success')
            button.classList.add('btn-warning')
            button.querySelector('span').textContent = 'Hizufügen-Form schlißen'
            icon.classList.remove('fa-arrow-right')
            icon.classList.add('fa-arrow-down')
            form.classList.remove('d-none')
            form.classList.add('d-block')
            
        } else {
            button.classList.remove('btn-warning')
            button.classList.add('btn-success')  
            button.querySelector('span').textContent = 'Hizufügen-Form öffnen'
            icon.classList.remove('fa-arrow-down')
            icon.classList.add('fa-arrow-right')  
            form.classList.remove('d-block')
            form.classList.add('d-none')          
        }
    }

    return (
        <div>
            <ToggleFormButton onFormularToggle={onFormularToggle}/>
            <AddProductForm onAdd={onAdd} kategorien={kategorien} onFormularToggle={onFormularToggle}/>
        </div>
    )
}

export default AddProduct
