
const ToggleFormButton = ({ onFormularToggle }) => {
    return (
        <button type="button" className="btn btn-block btn-success btn-form-toggle" onClick={onFormularToggle}>
            <span>Hizufügen-Form öffnen</span> <i className="fas fa-arrow-right"></i>
        </button>
    )
}

export default ToggleFormButton
