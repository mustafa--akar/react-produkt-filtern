
const DeleteButton = ({ id, onDelete }) => {
    const sicherFeldOffnen = () => {        
        document.querySelectorAll('.produkt-list-table tbody tr').forEach(row => {          
            if(row.querySelector('th').textContent === id) {
                row.querySelector('.sicher-felder').classList.remove('d-none')                
                row.querySelector('.loeschenButton').classList.add('d-none')
            }
        })  
    }

    const sicherFeldSchliessen = () => {        
        document.querySelectorAll('.produkt-list-table tbody tr').forEach(row => {            
            if(row.querySelector('th').textContent === id) {
                row.querySelector('.sicher-felder').classList.add('d-none')                
                row.querySelector('.loeschenButton').classList.remove('d-none')
            }
        })  
    }

    const deleteProdukt = () => {
        onDelete(id)
        document.querySelectorAll('.produkt-list-table tbody tr .buttons-container').forEach(container => {
            container.querySelector('.sicher-felder').classList.add('d-none')
            container.querySelector('.loeschenButton').classList.remove('d-none')
        })
    }


    return (
        <>
            <button type="button"                 
                onClick={sicherFeldOffnen}
                className="btn btn-warning btn-sm loeschenButton">
                Löschen
            </button>
            <div className="sicher-felder d-none">
                <span>Sicher?</span>
                <button type="button"                     
                    onClick={deleteProdukt}
                    className="btn btn-danger btn-sm sicherButton mr-1 ml-1">
                    Ja
                </button>
                <button type="button" 
                    onClick={sicherFeldSchliessen}
                    className="btn btn-info btn-sm nichtSicherButton">
                    Nein
                </button>
            </div>
        </>
    )
}

export default DeleteButton
