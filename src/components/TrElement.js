import DeleteButton from './DeleteButton'
import EditButton from './EditButton'

const TrElement = ({ product, onDelete }) => {
    return (
        <tr>
            <th scope="row">{product.id}</th>
            <td className="bezeichnung-feld">{product.bezeichnung}</td>
            <td className="preis-feld">{product.preis}</td>
            <td className="kategorie-feld">{product.kategorie}</td>
            <td className="buttons-container">
                <EditButton product={product}/>
                <DeleteButton id={product.id} onDelete={onDelete}/>
            </td>
        </tr>
    )
}

export default TrElement
