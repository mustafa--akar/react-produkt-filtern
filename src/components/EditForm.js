import AutocompleteInput from './AutocompleteInput'

const EditForm = ({ onEdit, kategorien }) => { 
    const formSchliessen = () => {
        document.querySelector('form.produkt-bearbeiten-form').classList.add('d-none')
        document.querySelector('table.produkt-list-table').classList.remove('d-none')
    }
    
    const onSubmit = event => {
        event.preventDefault()
        const id = document.querySelector('.produkt-bearbeiten-form input#produktId').value
        const bezeichnung = document.querySelector('.produkt-bearbeiten-form input#bezeichnung-bearbeiten').value
        const preis = document.querySelector('.produkt-bearbeiten-form input#preis-bearbeiten').value
        const kategorie = document.querySelector('.produkt-bearbeiten-form input#kategorie-bearbeiten').value
        const beschreibung = document.querySelector('.produkt-bearbeiten-form #beschreibung-bearbeiten').value

        onEdit({id, bezeichnung, beschreibung, preis, kategorie})

        document.querySelector('.produkt-bearbeiten-form').classList.add('d-none')
        document.querySelector('.produkt-list-table').classList.remove('d-none')
    }     

    return (
        <form className="mt-3 produkt-bearbeiten-form d-none" onSubmit={onSubmit} autoComplete="off">
            <div className="bearbeiten-form-kreuz" onClick={formSchliessen}>
                <i className="fas fa-times-circle"></i>
            </div>
            <div className="form-group">
                <label htmlFor="bezeichnung-bearbeiten">Bezeichnung</label>
                <input type="text"
                    // value={bezeichnung} 
                    // onChange={e => setBezeichnung(e.target.value)}
                    className="form-control" 
                    id="bezeichnung-bearbeiten" 
                    placeholder=""/>                      
            </div>  
            <div className="form-group">
                <label htmlFor="kategorie-bearbeiten">Kategorie</label>
                <AutocompleteInput id="kategorie-bearbeiten" kategorien={kategorien} onChange={() => {}}/>   
         
            </div>                    
            <div className="form-group">
                <label htmlFor="preis-bearbeiten">Preis</label>                      
                <input type="number" 
                    // value={preis} 
                    // onChange={event => setPreis(event.target.value)}                
                    min="1" 
                    step="any" 
                    className="form-control" 
                    id="preis-bearbeiten"/>                      
            </div>                     
            <div className="form-group">
                <label htmlFor="beschreibung-bearbeiten">Beschreibung</label>
                <textarea rows="5"
                    // value={beschreibung} 
                    // onChange={event => setBeschreibung(event.target.value)}                 
                    name="beschreibung" 
                    className="form-control" 
                    id="beschreibung-bearbeiten">
                </textarea>    
            </div>  
            <input type="hidden" value="" name="id" id="produktId"></input>          
            <button type="submit" className="btn btn-primary bearbeiten-form-submit mt-2">Bearbeiten</button>            
        </form>
    )
}

export default EditForm
