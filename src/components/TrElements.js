import TrElement from './TrElement'


const TrElements = ({ products, onDelete }) => {
    
    return (       
        <>
            {products.map((product, index) => (
                <TrElement 
                    key={index}
                    product={product}
                    onDelete={onDelete}/>
            ))} 
        </>
    )
}

export default TrElements
