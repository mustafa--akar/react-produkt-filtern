import { useState } from 'react'

const AutocompleteInput = ({ id, kategorien, onChange }) => {
    const [options, setOptions] = useState([])    
    
    const capitalFirstLetter = string => {
        return string.charAt(0).toUpperCase() + string.substring(1)
    }

    const pressKeyDown = () => {
        let acitiveContains = false
        let list = document.querySelectorAll(`#${id} + ul.kategorie-list li`)
        
        for(let element of list) {
            if(element.classList.contains('active')) {
                acitiveContains = true
                break
            }
        }        
        let firstChild = document.querySelector(`#${id} + ul.kategorie-list li:first-child`)
        if(acitiveContains === false && firstChild) {
            firstChild.classList.add('active')
            return
        }
        for(let element of list) {
            if(element.classList.contains('active')) {
                element.classList.remove('active')
                if(element.nextSibling) element.nextSibling.classList.add('active')
                break
            }
        }
    }
    const pressKeyUp = () => {
        const list = document.querySelectorAll(`#${id} + ul.kategorie-list li`)
        for(let li of list) {
            if(li.classList.contains('active')) {
                li.classList.remove('active')             
                if(li.previousSibling) li.previousSibling.classList.add('active')                
                break;
            }            
        }
    }
 
    const pressEnter = () => {        
        const list = document.querySelectorAll(`#${id} + ul.kategorie-list li`)
        const input = document.querySelector(`#${id}`)
     
        for(let li of list) { 
            if(li.classList.contains('active')) {
                onChange(li.textContent)                
                input.value = li.textContent
                setOptions([])        
            }
        }         
    }
    const clickEvent = event => {
        const input = document.querySelector(`#${id}`)
        onChange(event.target.textContent)
        input.value = event.target.textContent
        setOptions([])
    }
    
    const typeWords = (value) => {     
        if(value === '') return setOptions([])

        setOptions(kategorien.filter(cat => cat.includes(value)))      
    }
    const autoComplete = event => {    
        const valueWithUpperCase = capitalFirstLetter(event.target.value)      
        switch(event.key) {
            case 'ArrowDown'   : pressKeyDown(); break;
            case 'ArrowUp'     : pressKeyUp();   break;
            case 'Enter'       : pressEnter();   break;
            default            : typeWords(valueWithUpperCase);            
        }
    }  
 
    return (
        <>
            <input type="text"                 
                onChange={(event) => onChange(event.target.value)}
                onKeyUp={autoComplete}            
                className="form-control autocomplete" 
                id={id} 
                placeholder="" />   
            <ul className="kategorie-list">                      
                {options.map((option, index) => (
                    <li key={index} className="list-group-item" onClick={clickEvent}>{option}</li>
                ))}
            </ul>            
        </>
    )
}

export default AutocompleteInput
