import TrElements from './TrElements'

const ProductTable = ({ products, onDelete }) => {
    return (
        <table className="table table-hover produkt-list-table">
            <thead>
                <tr>
                    <th scope="col">#id</th>                        
                    <th scope="col">Bezeichnung</th>
                    <th scope="col">Preis</th>
                    <th scope="col">Kategorie</th>                    
                    <th scope="col">Operation</th>                    
                </tr>
            </thead>
            <tbody>                
                <TrElements products={products} onDelete={onDelete}/>
            </tbody>
        </table>
    )
}

export default ProductTable
