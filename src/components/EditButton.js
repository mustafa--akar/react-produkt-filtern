
const EditButton = ({ product }) => {
    const editFormOpen = product => {
        const editForm = document.querySelector('.produkt-bearbeiten-form')
        editForm.classList.remove('d-none')
        
        editForm.querySelector('input#produktId').value = product.id
        editForm.querySelector('input#bezeichnung-bearbeiten').value = product.bezeichnung
        editForm.querySelector('input#kategorie-bearbeiten').value = product.kategorie
        editForm.querySelector('input#preis-bearbeiten').value = product.preis
        editForm.querySelector('#beschreibung-bearbeiten').value = product.beschreibung

        document.querySelector('table.produkt-list-table').classList.add('d-none')
    }

    return (
        <button type="button" 
            onClick={() => editFormOpen(product)}             
            className="btn btn-secondary btn-sm bearbeitenButton">
            Bearbeiten        
        </button>
    )
}

export default EditButton
