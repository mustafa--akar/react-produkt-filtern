
const CategoryFilter = ({ kategorien, setCatFilter }) => {
    const entferneActiveClass = () => {
        // Alle active Class aus jedem List-Element entfernen
        for(let catRow of document.querySelectorAll('ul.ul-kategorie-list li')) {
            catRow.classList.remove('active')
        }        
    }
    const filterNachKategorien = (event) => {
        if(event.target.textContent === 'Alle Kategorien') setCatFilter('all')        
        else setCatFilter(event.target.textContent)

        entferneActiveClass()
        event.target.classList.add('active')
    }

    return (
        <>            
            <h3 className="mt-3">Kategorien</h3>
            <ul className="list-group ul-kategorie-list">
                <li 
                    onClick={filterNachKategorien}
                    className="list-group-item active">
                    Alle Kategorien
                </li>
                {kategorien.map((kategorie, index) => (
                    <li key={index} 
                        onClick={filterNachKategorien}
                        className="list-group-item">
                        {kategorie}
                    </li>
                ))}
            </ul>               
        </>
    )
}

export default CategoryFilter
