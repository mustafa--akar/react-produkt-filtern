import { useState } from 'react'
import AutocompleteInput from './AutocompleteInput'

const AddProductForm = ({ onAdd, kategorien, onFormularToggle}) => {
    const [bezeichnung, setBezeichnung] = useState('')
    const [kategorie, setKategorie] = useState('')
    const [preis, setPreis] = useState(0.01)
    const [beschreibung, setBeschreibung] = useState('')

    const onSubmit = event => {
        event.preventDefault();      
        
        onAdd({bezeichnung, beschreibung, preis, kategorie})
        
        if(bezeichnung && beschreibung && preis && kategorie) {
            setBezeichnung('')
            setKategorie('')
            setPreis(0)
            setBeschreibung('')
            onFormularToggle()  
            const input = document.querySelector('#kategorie-hinzufuegen')
            input.value = ''
        }

    } 

    return (
        <form className="mt-3 produkt-hinzufuegen-form d-none" onSubmit={onSubmit} autoComplete="off">

            <div className="form-group">
                <label htmlFor="bezeichnung-hinzufuegen">Bezeichnung</label>
                <input type="text" 
                    value={bezeichnung} 
                    onChange={event => setBezeichnung(event.target.value)}
                    className="form-control" 
                    id="bezeichnung-hinzufuegen" 
                    placeholder="" />                      
            </div>
            <div className="form-group">
                <label htmlFor="kategorie-hinzufuegen">Kategorie</label>
                <AutocompleteInput onChange={value => setKategorie(value)} id="kategorie-hinzufuegen" kategorien={kategorien}/>                   
            </div>   
            <div className="form-group">
                <label htmlFor="preis-hinzufuegen">Preis</label>                      
                <input type="number" 
                    value={preis} 
                    onChange={event => setPreis(event.target.value)}
                    min="0" 
                    step="any" 
                    className="form-control" 
                    id="preis-hinzufuegen"/>                      
            </div>                    
            <div className="form-group">
                <label htmlFor="beschreibung-hinzufuegen">Beschreibung</label>
                <textarea rows="5"
                    value={beschreibung} 
                    onChange={event => setBeschreibung(event.target.value)} 
                    name="beschreibung" 
                    className="form-control" 
                    id="beschreibung-hinzufuegen">
                </textarea>    
            </div>

            <button type="submit" className="btn btn-primary hinzufuegen-form-submit mt-2">Hinzufügen</button>                       
        </form>
    )
}

export default AddProductForm
