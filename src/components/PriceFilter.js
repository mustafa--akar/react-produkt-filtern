
const PriceFilter = ({ setPriceFilter }) => {
    const entferneActiveClass = () => {
        // Alle active Class aus jedem List-Element entfernen
        for(let preisRow of document.querySelectorAll('ul.ul-preis-list li')) {
            preisRow.classList.remove('active')
        }     
    }
    const filterNacPreis = event => {
        const minValue = parseInt(event.target.textContent.split('-')[0])
        const maxValue = parseInt(event.target.textContent.split('-')[1])
        
        entferneActiveClass()
        event.target.classList.add('active')

        if(event.target.textContent === 'Alle Preisklassen') setPriceFilter([])
        else setPriceFilter([minValue, maxValue])
    }
    return (
        <>
            <h3 className="mt-3">Preis</h3>
            <ul className="list-group ul-preis-list">
                <li onClick={filterNacPreis} className="list-group-item active">Alle Preisklassen</li>
                <li onClick={filterNacPreis} className="list-group-item">0€ - 20€</li>
                <li onClick={filterNacPreis} className="list-group-item">20€ - 40€</li>
                <li onClick={filterNacPreis} className="list-group-item">40€ - 60€</li>
                <li onClick={filterNacPreis} className="list-group-item">60€ - 80€</li>
                <li onClick={filterNacPreis} className="list-group-item">80€ - 100€</li>                    
            </ul>             
        </>
    )
}

export default PriceFilter
